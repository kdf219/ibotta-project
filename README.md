Ibotta Dev Project -- Kelli Frank
===============

## Deployment
My project was created using maven for deployment. It is set to run on port 3000.
It can be deployed using the following commands:
```aidl
cd platform_dev/api
mvn package
mvn exec:java
```

##Endpoints
The project contains all the required endpoints as well as the optional ones
- `POST /words.json`: Takes a JSON array of English-language words and adds them to the corpus (data store).
- `GET /anagrams/:word.json?{properNouns}{limit}`:
  - Returns a JSON array of English-language words that are anagrams of the word passed in the URL.
  - This endpoint should support an optional query param that indicates the maximum number of results to return.
  - Both properNouns and limit are optional query parameters. `properNouns` accepts teh values `true` and `false`. `limit` accepts an integer.
- `DELETE /words/:word.json`: Deletes a single word from the data store.
- `DELETE /words.json`: Deletes all contents of the data store.


**Optional Endoints**
- `GET /words/count`: Endpoint that returns a count of words in the corpus and min/max/median/average word length
- `GET /anagrams/most`: Endpoint that identifies words with the most anagrams
- `GET /anagrams/eachOther`: Endpoint that takes a set of words and returns whether or not they are all anagrams of each other
- `GET /words/size/:size`: Endpoint to return all anagram groups of size >= *x*
- `DELETE /words/anagrams/:words.json`: Endpoint to delete a word *and all of its anagrams*

##Documentation
I used a HashMap to store words mapped to their letters and their letters' counts. This makes it so that anagrams can be accessed in constant time once the counts of a word are known. I currently have this mapped in one direction but I could have an additional HashMap that stores this information so that the counts would only have to be calculated once for each word. At this time most words are short enough that I do not believe the space this would take up would outweigh the additional time it takes to calculate it.