package kellifrank;

import com.google.gson.Gson;
import spark.Spark;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class App {
    private static HashSet<String> dictionary = new HashSet<>();
    private static HashMap<HashMap<Character, Integer>, HashSet<String>> perms = new HashMap<>();

    /**
     * Adds all words in a file to the dictionary
     *
     * @param fileName file to be examined
     * @throws IOException if file not found
     */
    static void fileToDictionary(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));

        String curr;
        while ((curr = br.readLine()) != null) {
            addWord(curr);
        }
    }

    /**
     * Adds a word to the perm list
     *
     * @param word word to be added
     *             O(1)
     */
    private static void addToPerms(String word) {// iterate through curr -- add letter counts and store word
        // assumes proper nouns are different than non proper even if same spelling
        HashMap<Character, Integer> counts = getCounts(word.toLowerCase());

        HashSet<String> words = perms.get(counts);

        if (words == null) {
            words = new HashSet<>();
        }

        words.add(word);
        perms.put(counts, words);
    }

    /**
     * Removes a word from the perm hash
     *
     * @param word word to be removed
     *             O(1)
     */
    private static void removeWord(String word) {
        HashMap<Character, Integer> counts = getCounts(word);
        HashSet<String> words;

        if (perms.get(counts) != null) {
            words = perms.get(counts);
        } else {
            return; // not in the set to begin with
        }

        words.remove(word);
        perms.put(counts, words);

        dictionary.remove(word);
    }

    /**
     * Gets the counts of letters in a word
     *
     * @param curr word to be examined
     * @return HashMap with each character mapped to its count
     * O(n) -- n length of the word
     */
    static HashMap<Character, Integer> getCounts(String curr) {
        // iterate through curr -- add letter counts and store word
        HashMap<Character, Integer> counts = new HashMap<>();
        for (int i = 0; i < curr.length(); i++) {
            char letter = curr.charAt(i);
            counts.merge(letter, 1, (a, b) -> a + b); //increment count or insert letter
        }
        return counts;
    }

    public static void main(String[] args) throws IOException {
//        fileToDictionary("dictionary.txt");
        final Gson gson = new Gson();
        Spark.port(3000);

        // adds json array of words to the dictionary
        // O(n) -- n number of words
        Spark.post("/words.json", (req, res) -> {
            WordList words = gson.fromJson(req.body(), WordList.class);
            dictionary.addAll(words.words);
            words.words.forEach(App::addToPerms); // add each word to perms
            res.status(201);
            return "";
        });

        // gets anagrams of word
        // O(n)
        Spark.get("anagrams/:word.json", (req, res) -> {
            int limit = Integer.parseInt(req.queryParamOrDefault("limit", dictionary.size() + ""));
            boolean includeProper = !Boolean.parseBoolean(req.queryParamOrDefault("properNouns", "false"));
            String word = req.params("word.json");
            word = word.substring(0, word.lastIndexOf(".json"));
            return gson.toJson(new AnagramList(getAnagrams(word, limit, !includeProper)));
        });

        // deletes word
        // O(1)
        Spark.delete("words/:word.json", (req, res) -> {
            // returns existing number
            String word = req.params("word.json");
            word = word.substring(0, word.indexOf(".json"));
            removeWord(word);
            res.status(204);
            return "";
        });

        // deletes a word and all anagrams of that word
        // O(n) - n is number of anagrams for a word -- worst case would be number of total words if all anagrams of each other
        Spark.delete("words/anagrams/:word.json", (req, res) -> {
            String word = req.params("word.json");
            word = word.substring(0, word.indexOf(".json"));
            HashSet<String> anagrams = perms.get(getCounts(word));
            removeWord(word);
            for (String anagram : anagrams) {
                removeWord(anagram);
            }
            res.status(204);
            return "";
        });

        // checks if a list of words are anagrams of each other
        // O(n) - n is number of words in list
        Spark.get("/anagrams/eachOther", (req, res) -> {
            WordList words = gson.fromJson(req.body(), WordList.class);
            return anagramsOfEachOther(words);
        });

        // deletes all words
        // O(1)
        Spark.delete("/words.json", (req, res) -> {
            setPerms(new HashMap<>());
            dictionary = new HashSet<>(); // clear dictionary
            res.status(204);
            return dictionary.size();
        });

        // gets count of words
        // O(n) -- n is number of wors
        Spark.get("/words/count", (req, res) -> {
            boolean max = Boolean.parseBoolean(req.queryParams("max"));
            boolean min = Boolean.parseBoolean(req.queryParams("min"));
            boolean avg = Boolean.parseBoolean(req.queryParams("average"));

            int maxSize = 0, minSize = 999, avgSize = 0;
            for (String word : dictionary) {
                int currLen = word.length();
                avgSize += currLen;
                if (currLen > maxSize) {
                    maxSize = currLen;
                }
                if (currLen < minSize) {
                    minSize = currLen;
                }
            }
            avgSize /= dictionary.size();

            HashMap<String, Integer> response = new HashMap<>();
            response.put("dictionary size", dictionary.size());

            if (max) {
                response.put("max size", maxSize);
            }

            if (min) {
                response.put("min size", minSize);
            }

            if (avg) {
                response.put("average size", avgSize);
            }

            return response;
        });

        // gets words with most anagrams
        // O(n) -- n is number of words
        Spark.get("/anagrams/most", (req, res) -> {
            final int[] max = {1};
            AtomicReference<HashSet<String>> maxes = new AtomicReference<>(new HashSet<>());
            perms.forEach((map, set) -> {
                if (set.size() > max[0]) {
                    max[0] = set.size();
                    maxes.set(set);
                }
            });
            return maxes;
        });

        // gets words with more than some number of anagrams
        // O(n) -- n number of words
        Spark.get("/words/size/:size", (req, res) -> {
            int size = Integer.parseInt(req.params("size"));
            ArrayList<AnagramList> valid = new ArrayList<>();
            perms.forEach((map, set) -> {
                if (set.size() >= size) {
                    valid.add(new AnagramList(set));
                }
            });
            return gson.toJson(valid, AnagramList.class);
        });

        Scanner input = new Scanner(System.in);
        while (!input.nextLine().equals("quit")) ;
        System.exit(0);

    }

    // checks if all words in a list are anagrams of each other
    // O(n) - n is number of words in list
    static boolean anagramsOfEachOther(WordList words) {
        HashSet<String> equal = new HashSet<>();

        for (String word : words.words) {
            HashSet<String> temp = perms.get(getCounts(word));
            if (temp == null) {
                temp = new HashSet<>();
            }

            if (equal.size() == 0) {
                equal = temp;
            }

            if (!equal.containsAll(temp) || !temp.containsAll(equal)) {
                return false;
            }

            equal = temp;
        }
        return true;
    }

    /**
     * Gets all anagrams of a word
     *
     * @param word  to find anagrams
     * @param limit max number of anagrams to find
     * @return anagrams of this word
     * O(n) -- n is number of words
     */
    static HashSet<String> getAnagrams(String word, int limit, boolean excludeProper) {
        HashSet<String> anagrams = perms.get(getCounts(word));

        if (anagrams == null) {
            anagrams = new HashSet<>();
        }

        anagrams.remove(word);

        if (limit < dictionary.size()) {
            HashSet<String> limitedAnagrams = new HashSet<>();
            int count = 0;
            for (String entry : anagrams) {
                if (excludeProper) { // if excluding proper nouns
                    if (entry.equals(entry.toLowerCase())) { // if not a proper noun, add it
                        limitedAnagrams.add(entry);
                    } else {
                        continue; // next word if proper noun
                    }
                } else {
                    // if including proper nouns add all
                    limitedAnagrams.add(entry);
                }
                count++;
                if (count >= limit || limit >= dictionary.size()) {
                    return limitedAnagrams;
                }
            }
        }

        HashSet<String> notProper = new HashSet<>();
        if (excludeProper) {
            for (String anagram : anagrams) {
                if (anagram.equals(anagram.toLowerCase())) { // not proper noun
                    notProper.add(anagram);
                }
            }
            return notProper;
        }

        return anagrams;
    }

    // adds a word to the dictionary and inputs it into the map with its counts
    // O(1)
    public static void addWord(String word) {
        dictionary.add(word);
        addToPerms(word);
    }

    public static HashSet<String> getDictionary() {
        return dictionary;
    }

    public static HashMap<HashMap<Character, Integer>, HashSet<String>> getPerms() {
        return perms;
    }

    public static void setPerms(HashMap<HashMap<Character, Integer>, HashSet<String>> perms) {
        if (perms == null) {
            App.perms = new HashMap<>();
        } else {
            App.perms = perms;
        }
    }

    static class WordList {
        ArrayList<String> words;

        public WordList() {
            words = new ArrayList<>();
        }
    }

    static class AnagramList {
        HashSet<String> anagrams;

        public AnagramList() {
            anagrams = new HashSet<>();
        }

        AnagramList(HashSet<String> list) {
            anagrams = list;
            if (anagrams == null) {
                anagrams = new HashSet<>();
            }
        }
    }

}