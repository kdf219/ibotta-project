package kellifrank;

import com.google.gson.Gson;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) throws IOException {
        super(testName);

        App.addWord("Tuna");
        App.addWord("tuna");
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }


    /**
     * Tests that getting with and without proper nouns returns different lists
     */
    public void testProperVsNotProper() {
        HashSet<String> withProper = App.getAnagrams("tuna", -1, false);
        HashSet<String> withoutProper = App.getAnagrams("tuna", -1, true);
        assertNotSame(withoutProper, withProper);
    }

    public void testNotProperAllLowerCase() {
        HashSet<String> withoutProper = App.getAnagrams("tuna", -1, true);
        for (String word : withoutProper) {
            // ensures every word in without proper is not proper
            assertSame(word, word.toLowerCase());
        }
    }

    public void testAnagramsOfEachOther() {
        App.addWord("organ");
        App.addWord("groan");
        App.addWord("orang");

        final Gson gson = new Gson();
        App.WordList words = gson.fromJson(
                "{\"words\":[\"organ\", \"groan\", \"orang\"]}"
                , App.WordList.class);
        assertTrue(App.anagramsOfEachOther(words));

        words = gson.fromJson(
                "{\"words\":[\"organ\", \"groan\", \"orng\"]}"
                , App.WordList.class);
        assertFalse(App.anagramsOfEachOther(words));

    }

    public void testGetCounts() {
        App.addWord("one");
        HashMap<Character, Integer> counts = App.getCounts("one");

        assertTrue(counts.get('o').equals(1));
        assertTrue(counts.get('n').equals(1));
        assertTrue(counts.get('e').equals(1));
        assertTrue(counts.size() == 3);
    }


}
